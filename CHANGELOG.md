# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/).

## [Unreleased]

### fixed
- Shutdowns handle failed device close requests, which make client restarts more dependable.

### Added
- initial release
